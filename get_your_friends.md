
Dans la console du navigateur, sur la page https://www.facebook.com/<FACEBOOK_USER_NAME>/following, exécuter (et patienter jusqu'à ce que toute la page ait été déroulée) :

```
(function() {
    var intervalObj = null;
    var retry = 0;
    var clickHandler = function() {
        console.log("Clicked; stopping autoscroll");
        clearInterval(intervalObj);
        document.body.removeEventListener("click", clickHandler);
    }
    function scrollDown() {
        var scrollHeight = document.body.scrollHeight,
            scrollTop = document.body.scrollTop,
            innerHeight = window.innerHeight,
            difference = (scrollHeight - scrollTop) - innerHeight

        if (difference > 0) {
            window.scrollBy(0, difference);
            if (retry > 0) {
                retry = 0;
            }
            console.log("scrolling down more");
        } else {
            if (retry >= 3) {
                console.log("reached bottom of page; stopping");
                clearInterval(intervalObj);
                document.body.removeEventListener("click", clickHandler);
            } else {
                console.log("[apparenty] hit bottom of page; retrying: " + (retry + 1));
                retry++;
            }
        }
    }

    document.body.addEventListener("click", clickHandler);

    intervalObj = setInterval(scrollDown, 1000);

})()

```

Puis lorsque le scroll a été réalisé, exécuter le script suivant. Un fichier sera proposé au téléchargement, enregistrez-le dans le dossier racine de votre répo.

```

elements = document.querySelectorAll("a.oajrlxb2.g5ia77u1.qu0x051f.esr5mh6w.e9989ue4.r7d6kgcz.rq0escxv.nhd2j8a9.nc684nl6.p7hjln8o.kvgmc6g5.cxmmr5t8.oygrvhab.hcukyx3x.jb3vyjys.rz4wbd8a.qt6c0cv9.a8nywdso.i1ao9s8h.esuyzwwr.f1sip0of.lzcic4wl.gmql0nx0.gpro0wi8")

content = []
for (el of elements){
try {
   content.push(el.innerText + "," + el.href + "\n")
                }
catch{
console.log("error")
}
}

blob = new Blob(content);
url = URL.createObjectURL(blob);
file = document.createElement(`a`);
file.download = `friends.csv`;
file.href = url;
document.body.appendChild(file);
file.click();
file.remove();
URL.revokeObjectURL(url);

```

Vous pouvez désormais faire usage de votre liste de contacts.
