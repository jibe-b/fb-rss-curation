from requests import get, post
from json import loads, dumps
from atoma import parse_atom_bytes
from bs4 import BeautifulSoup 
from re import findall, search
from os.path import exists
from subprocess import run, DEVNULL
from re import sub
from csv import reader
from datetime import datetime

def fetch_rss_host():
    response = get(
        url = "https://api.airtable.com/v0/app3iXlJIyrGDdOvK/rss-host",
        headers = {"Authorization": "Bearer {}".format(airtable_api_key)}
    )

    response_json = loads(response.content)
    hosts = response_json["records"]

    return hosts

def fetch_sources():
    sources = get(
        url = "https://api.airtable.com/v0/app3iXlJIyrGDdOvK/sources",
        headers = {"Authorization": "Bearer {}".format(airtable_api_key)}
    )
    return sources

def get_posts_from_facebook_sources(hosts, sources):
    user_agent = "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:56.0) Gecko/20100101 Firefox/56.0"
    
    cached_links = [row[0] for row in reader(open('cached_links.txt', newline=''))]
    
    all_posts = {}

    for source_field in loads(sources.content)["records"]:
        source = source_field["fields"]["url"]
        
        if search(r"/$", source):
            source = "/".join(source.split("/")[:-1])

        posts = []

        if len(findall("/groups/", source)) > 0:
            context = "Group"
            uri_key = "g"
            source_url = source
        else:
            context = "User"
            uri_key = "u"
            source_url = source.split("/")[-1]
            
        #i = 0
        #continue_ = True
        
        for host_dict in hosts:
            host = host_dict["fields"]["url"]
            url_to_get = "{}?action=display&bridge=Facebook&limit=-1&format={}&context={}&{}={}".format(host, "Json", context, uri_key, source_url)
                        
            try:
                response = get(url_to_get)
                feed = loads(response.content)["items"]
                if search("Bridge returned error 500!", feed[0]["title"]) != None:
                    feed = []
                else:
                    print("Feed found through", host, "for", source_url)
                    hosts.remove(host_dict)
                    hosts = [host_dict] + hosts
                    break
            except:
                feed = []
            
        if len(feed) == 0:
            open("failed_sources.txt", 'a').write(source_url + "\n")
            print("no bridge found for", source_url)

        for entry in feed:               
            id_ = entry["id"]
            if "author" in entry.keys():
                author = entry["author"]["name"]
            else:
                author = ""

            updated = datetime.strftime(datetime.now(), "%d/%m/%Y à %H:%M:%S")
            title = entry["title"]
            
            if "url" in entry.keys():
                url = entry["url"]
            else:
                url = ""
                
            html = BeautifulSoup(entry["content_html"], features="lxml")
            
            if "attachments" in entry.keys():
                attachments = entry["attachments"]
            else:
                attachments = ""

            # store pictures only once if post is shared (post with id_ in "urn" will have to read the same picture local path)
            if len(findall("permalink", id_)) > 0:
                permalink = id_.split("permalink")[1].split("/")[1]
            elif len(findall("posts", id_)) > 0:
                permalink = id_.split("posts")[1].split("/")[1]
            elif len(findall("photos", id_)) > 0:
                permalink = id_.split("photos")[1].split("/")[1]
            else:
                permalink = id_

            if context == "Group":
                pictures_urls = attachments[1:]
            elif context == "User":
                pictures_urls = [item["src"] for item in html.findAll("img")]

            pictures_local_path = []
            for index, link_dict in enumerate(pictures_urls):
                link = link_dict["url"]

                if len(findall("url", link)) > 0:
                    l = link.split("url=")[1]
                else:
                    l = link
                image_filename = "{}-{}.png".format(permalink, index)
                image_directory = "pictures"
                local_path = "{}/{}".format(image_directory, image_filename)
                pictures_local_path.append(local_path)

                if not exists(local_path):
                    run(["wget", "-U", user_agent, l, "-O", local_path], stderr=DEVNULL)
                else:
                    pass # print("in cache")

            item = {
                "id_": id_,
                "author": author,
                "updated": updated,
                "title": title,
                "url": url,
                "content": str(html),
                "source": source,
                "pictures_local_path": ",".join(pictures_local_path)
            }
            posts.append(item)

            # POST

            if id_ not in cached_links and search("https://", id_):
                print(id_)
                response = post("https://api.airtable.com/v0/app3iXlJIyrGDdOvK/posts",
                    headers={
                         "Authorization": "Bearer {}".format(airtable_api_key),
                         "Content-Type": "application/json"
                    },
                    data=dumps({"records": [{"fields": item}]})
                )

                open("cached_links.txt", 'a').write("{}\n".format(id_))
        #except:
        #    print("failed parsing")
            
        all_posts[source] = posts

    return all_posts

airtable_api_key = open("airtable_api_key", 'r').read()[:-1]
hosts = fetch_rss_host()
sources = fetch_sources()
get_posts_from_facebook_sources(hosts, sources)
