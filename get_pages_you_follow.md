Dans la console du navigateur, sur la page https://www.facebook.com/<FACEBOOK_USER_NAME>/following, exécuter (et patienter jusqu'à ce que toute la page ait été déroulée) :

```
(function() {
    var intervalObj = null;
    var retry = 0;
    var clickHandler = function() { 
        console.log("Clicked; stopping autoscroll");
        clearInterval(intervalObj);
        document.body.removeEventListener("click", clickHandler);
    }
    function scrollDown() { 
        var scrollHeight = document.body.scrollHeight,
            scrollTop = document.body.scrollTop,
            innerHeight = window.innerHeight,
            difference = (scrollHeight - scrollTop) - innerHeight

        if (difference > 0) { 
            window.scrollBy(0, difference);
            if (retry > 0) { 
                retry = 0;
            }
            console.log("scrolling down more");
        } else {
            if (retry >= 3) {
                console.log("reached bottom of page; stopping");
                clearInterval(intervalObj);
                document.body.removeEventListener("click", clickHandler);
            } else {
                console.log("[apparenty] hit bottom of page; retrying: " + (retry + 1));
                retry++;
            }
        }
    }

    document.body.addEventListener("click", clickHandler);

    intervalObj = setInterval(scrollDown, 1000);

})()

```

Puis lorsque le scroll a été réalisé, exécuter le script suivant. Un fichier sera proposé au téléchargement, enregistrez-le dans le dossier racine de votre répo.


```
content = []
for (el of document.querySelectorAll("div.bp9cbjyn.ue3kfks5.pw54ja7n.uo3d90p7.l82x9zwi.n1f8r23x.rq0escxv.j83agx80.bi6gxh9e.discj3wi.hv4rvrfc.ihqw7lf3.dati1w0a.gfomwglr") ){
try{
  content.push(
el.querySelector("a").href + "," + el.querySelectorAll("span")[1].innerText + "\n")
}catch{
console.log("error")
}
}

blob = new Blob(content);
url = URL.createObjectURL(blob);
file = document.createElement(`a`);
file.download = `following.csv`;
file.href = url;
document.body.appendChild(file);
file.click();
file.remove();
URL.revokeObjectURL(url);

```

Vous pouvez désormais faire usage du tableau des pages que vous suivez.
